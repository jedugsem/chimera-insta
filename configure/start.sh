apk add pipewire bluez seatd dbus ufw git zsh base-cbuild-host bmake pkgconf neovim 
dinictl enable bluetoothd
dinictl enable iwd
dinictl enable seatd
dinictl start wireplumber
dinictl start pipewire-pulse

# Firewall
dinitctl enable ufw
ufw enable 
ufw default deny incoming
ufw default allow outgoing
ufw allow ssh http https
ufw limit ssh

# remeber crony dhcpd

# Graphical 
apk add swaybg sway  mesa mesa-dri mesa-vulkan foot firefox gimp mpv inkscape htop fnott cupsevince

# cutom Packages
# typst snapper tt-firacode-nerd 
#

river typst kikad freecad librecad neomutt telegram pmbootstrap xplr zathura imagewriter bemenu tofi system-config-printer 
Archive package
