#!/bin/sh
. ./vars
. ./disk.sh

pass=$(/lib/cryptsetup/askpass "Provide the encryption password: ")
userpass=$(/lib/cryptsetup/askpass "Provide root and user password: ")

create_part

first_mount

chimera-bootstrap -f $MOUNT


cp -r ../../chimera-insta /mnt/tmp/chimera-insta
chimera-chroot $MOUNT /bin/sh /tmp/chimera-insta/install/chroot/main.sh $userpass

#sleep 0.5
#umount -Rl $MOUNT
#cryptsetup close $CRYPT
#umount $MOUNT

# reinstall
# apk add chimera-repo-contrib
# apk update
# apk add lsof
# dmsetup ls
# lsof | grep x,z
# kill -9 id 
