#!/bin/sh
# Needed are

create_part() {
    #fdisk
    echo $pass | cryptsetup -q luksFormat --type luks1 --iter-time $ITER $ROOT
    #cryptsetup luksFormat --type luks2 -- --iter-time $ITER $ROOT
    echo $pass | cryptsetup open $ROOT $CRYPT
    umount -Rf $MOUNT
    rm -rf ${MOUNT}/*
    mkfs.vfat -F32 "$EFI"
    mkfs.btrfs -f -L VERA -n 32k $CD
    #mount -o "$MOP" $CD ${MOUNT}/
    mount $CD $MOUNT

    btrfs subvolume create ${MOUNT}/@
    
    mkdir ${MOUNT}/@/boot
    btrfs subvolume create ${MOUNT}/@/boot/grub
    btrfs subvolume create ${MOUNT}/@/.snapshots
    
    mkdir ${MOUNT}/@/.snapshots/1
    btrfs subvolume create ${MOUNT}/@/.snapshots/1/snapshot
    btrfs subvolume create ${MOUNT}/@/home
    btrfs subvolume create ${MOUNT}/@/opt
    btrfs subvolume create ${MOUNT}/@/root
    btrfs subvolume create ${MOUNT}/@/srv
    btrfs subvolume create ${MOUNT}/@/tmp
    
    mkdir ${MOUNT}/@/usr/
    btrfs subvolume create ${MOUNT}/@/usr/local
    
    mkdir ${MOUNT}/@/var/
    btrfs subvolume create ${MOUNT}/@/var/cache
    btrfs subvolume create ${MOUNT}/@/var/log
    btrfs subvolume create ${MOUNT}/@/var/spool
    btrfs subvolume create ${MOUNT}/@/var/tmp
    btrfs subvolume create ${MOUNT}/@/var/lib
    btrfs subvolume create ${MOUNT}/@/var/swap

    chattr +C ${MOUNT}/@/var/lib
    chattr +C ${MOUNT}/@/var/spool
    chattr +C ${MOUNT}/@/var/cache
    chattr +C ${MOUNT}/@/var/tmp
    chattr +C ${MOUNT}/@/var/log
    chattr +C ${MOUNT}/@/var/swap

    echo "<?xml version=\"1.0\"?>
    <snapshot>
        <type>single</type>
        <num>1</num>
        <date>$(date +"%Y-%m-%d %H:%M:%S")</date>
        <description>First Snapshot</description>
    </snapshot>" > ${MOUNT}/@/.snapshots/1/info.xml

    btrfs subvolume set-default $(btrfs subvolume list ${MOUNT} | grep "@/.snapshots/1/snapshot" | sed 's/ID //' | sed 's/ gen.*//') /mnt
    umount $MOUNT
}

first_mount() {
    echo pre_mount 

    mount -o $MOP $CD ${MOUNT}
    mkdir -p ${MOUNT}/boot/grub
    mkdir ${MOUNT}/.snapshots
    mkdir ${MOUNT}/home
    mkdir ${MOUNT}/opt
    mkdir ${MOUNT}/root
    mkdir ${MOUNT}/srv
    mkdir ${MOUNT}/tmp
    mkdir -p ${MOUNT}/usr/local
    mkdir ${MOUNT}/var
    mkdir ${MOUNT}/var/spool
    mkdir ${MOUNT}/var/lib
    mkdir ${MOUNT}/var/log
    mkdir ${MOUNT}/var/cache
    mkdir ${MOUNT}/var/tmp
    mkdir ${MOUNT}/var/swap

    mount_part

    dd bs=512 count=4 if=/dev/random of=${MOUNT}/crypto_keyfile.bin iflag=fullblock
    chmod 000 ${MOUNT}/crypto_keyfile.bin
    echo $pass | cryptsetup -v luksAddKey $ROOT ${MOUNT}/crypto_keyfile.bin

}

mount_part() {

    echo "middle mount"

    mount -o "$MOP",subvol=@/.snapshots $CD ${MOUNT}/.snapshots 
    mount -o "$MOP",subvol=@/home $CD ${MOUNT}/home 
    mount -o "$MOP",subvol=@/opt $CD ${MOUNT}/opt
    mount -o "$MOP",subvol=@/root $CD ${MOUNT}/root 
    mount -o "$MOP",subvol=@/srv $CD ${MOUNT}/srv
    mount -o "$MOP",subvol=@/tmp $CD ${MOUNT}/tmp 
    mount -o "$MOP",subvol=@/usr/local $CD ${MOUNT}/usr/local
    mount -o "$MOP",subvol=@/var/cache $CD ${MOUNT}/var/cache
    mount -o "$MOP",subvol=@/var/log $CD ${MOUNT}/var/log
    mount -o "$MOP",subvol=@/var/spool $CD ${MOUNT}/var/spool
    mount -o "$MOP",subvol=@/var/lib $CD ${MOUNT}/var/lib
    mount -o "$MOP",subvol=@/var/tmp $CD ${MOUNT}/var/tmp
    mount -o subvol=@/var/swap $CD ${MOUNT}/var/swap
    mount -o "$MOP",subvol=@/boot/grub $CD ${MOUNT}/boot/grub

    echo "efi mount"
    mkdir -p ${MOUNT}/boot/efi/
    mount $EFI ${MOUNT}/boot/efi
}

