#!/bin/sh
. ./vars
. ./disk.sh
pass=$(/lib/cryptsetup/askpass "Provide the encryption password: ")

echo $pass | cryptsetup open $ROOT $CRYPT

mount -o $MOP $CD ${MOUNT}

mount_part

chimera-chroot $MOUNT
