create_swap() {
  truncate -s 0 /var/swap/swapfile
  chattr +C /var/swap/swapfile
  fallocate -l 8G /var/swap/swapfile

  chmod 600 /var/swap/swapfile
  mkswap /var/swap/swapfile
  swapon /var/swap/swapfile

  echo "vm.swappiness=10" >> /etc/sysctl.conf
}

setup_snapper() {
  apk add --no-interactive --allow-untrusted "/tmp/chimera-insta/install/chroot/snapper-0.10.6-r0.apk"
  umount /.snapshots
  rm -r /.snapshots
  snapper --no-dbus -c root create-config /
  btrfs subvolume delete /.snapshots
  mkdir /.snapshots
  mount -o compress=zstd,subvol=@/.snapshots $CD /.snapshots
  chmod 750 /.snapshots
}

setup_grub() {
  apk add --no-interactive grub-x86_64-efi
  
  # Grub
  sed -i -e "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
  sed -i -e "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux

  echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
  echo "GRUB_CMDLINE_LINUX_DEFAULT=\"cryptdevice=UUID=${UUID}:cryptroot:allow-discards resume=/dev/mapper/$CRYPT resume_offset=${roffset}\"" >> /etc/default/grub


  # Initramfs
  echo "KEYFILE_PATTERN=\"/crypto_keyfile.bin\"" >>/etc/cryptsetup-initramfs/conf-hook
  #echo "RESUME=UUID=${LUKS_UUID}" > /etc/initramfs-tools/conf.d/resume
  echo UMASK=0077 >>/etc/initramfs-tools/initramfs.conf
  
  update-initramfs -c -k all
  
  grub-install --target=x86_64-efi --efi-directory=/boot/efi
   
  grub-mkconfig -o /boot/grub/grub.cfg

}

fstab () {
    echo "cryptroot UUID=${UUID} /crypto_keyfile.bin luks,discard,key-slot=1" > /etc/crypttab
    echo "#
# See fstab(5).
#
# <file system>	<dir>	<type>	<options>		<dump>	<pass>
tmpfs		/tmp	tmpfs	defaults,nosuid,nodev   0       0
UUID=${LUKS_UUID} / btrfs rw,$MOP 0 0
UUID=${LUKS_UUID} /home btrfs rw,$MOP,subvol=/@/home 0 0
UUID=${LUKS_UUID} /opt btrfs rw,$MOP,subvol=/@/opt 0 0
UUID=${LUKS_UUID} /root btrfs rw,$MOP,subvol=/@/root 0 0
UUID=${LUKS_UUID} /srv btrfs rw,$MOP,subvol=/@/srv 0 0
UUID=${LUKS_UUID} /tmp btrfs rw,$MOP,subvol=/@/tmp 0 0
UUID=${LUKS_UUID} /usr/local btrfs rw,$MOP,subvol=@/usr/local 0 0
UUID=${LUKS_UUID} /var/cache btrfs rw,$MOP,subvol=@/var/cache 0 0
UUID=${LUKS_UUID} /var/log btrfs rw,$MOP,subvol=@/var/log 0 0
UUID=${LUKS_UUID} /var/spool btrfs rw,$MOP,subvol=@/var/spool 0 0
UUID=${LUKS_UUID} /var/lib btrfs rw,$MOP,subvol=@/var/lib 0 0
UUID=${LUKS_UUID} /var/tmp btrfs rw,$MOP,subvol=@/var/tmp 0 0
UUID=${LUKS_UUID} /boot/grub btrfs rw,$MOP,subvol=@/boot/grub 0 0
UUID=${LUKS_UUID} /.snapshots btrfs rw,$MOP,subvol=@/.snapshots 0 0
UUID=${LUKS_UUID} /var/swap btrfs rw,subvol=@/var/swap 0 0

UUID=${EFI_UUID} /boot/efi vfat defaults 0 0

/var/swap/swapfile    none    swap    defaults    0   0
  " > /etc/fstab
}
