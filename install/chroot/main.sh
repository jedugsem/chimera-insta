#!/bin/sh
. /tmp/chimera-insta/install/vars
. /tmp/chimera-insta/install/chroot/funcs.sh
LUKS_UUID=$(blkid -s UUID -o value $CD)
UUID=$(blkid -s UUID -o value $ROOT)
EFI_UUID=$(blkid -s UUID -o value $EFI)

echo "fstab" 
fstab 

echo "update and additions"
apk update
apk upgrade --available
apk add --no-interactive linux-stable cryptsetup-scripts chimera-repo-contrib iwd dhcpcd ucode-amd

echo "creating swap"
create_swap

roffset=$(btrfs inspect-internal map-swapfile -r /var/swap/swapfile)

echo "setting up snapper"
setup_snapper

echo "now grub for the win"
setup_grub
apk add --no-interactive git neovim


echo "Post Installation"


echo $HOSTNAME > /etc/hostname
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

echo -e "$1\n$1" | passwd 
useradd $USERNAME
usermod -a -G wheel,kvm,network $USERNAME
echo -e "$1\n$1" | passwd $USERNAME


# Disable Beep
echo "blacklist pcspkr" >> /etc/modprobe.d/dont_load.conf

dinitctl -o enable iwd
dinitctl -o enable dhcpcd 

sed -i -e "s/KMAP=us/KMAP=de-latin1/g" /etc/default/keyboard

mv /tmp/chimera-insta/configure /home/${USERNAME}/config-scripts
rm -rf /tmp/chimera-insta

